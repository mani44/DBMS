var mongoose = require("mongoose");

mongoose.set("useCreateIndex", true);

const carSchema = mongoose.Schema({
    brand: String,
    name: [String],
    image: [String]
});

module.exports = mongoose.model("Car", carSchema);